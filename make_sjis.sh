#!/bin/sh

mkdir -p sjis

for f in $(cd utf8 && ls *.bib *.sty *.tex); do
	nkf -s -Lw utf8/$f > sjis/$f
done

cp utf8/*.pdf sjis/

